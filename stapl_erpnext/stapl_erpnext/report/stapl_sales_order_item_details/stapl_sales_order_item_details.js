// Copyright (c) 2016, Castlecraft Ecommerce Pvt. Ltd. and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["STAPL Sales Order Item Details"] = {
	"filters": [
		{
			fieldname:"sales_order",
			label: __("Sales Order"),
			fieldtype: "Link",
			width: "80",
			options: "Sales Order",
		},

	]
};
