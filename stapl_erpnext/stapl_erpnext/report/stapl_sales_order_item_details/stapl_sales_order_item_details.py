# Copyright (c) 2013, Castlecraft Ecommerce Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe

def execute(filters=None):
	columns = get_columns(filters)
	data = get_result(filters)
	
	return columns, data

def get_columns(filters):
	columns = [
		{"label": ("Sales Order"), "fieldname": "sales_order", "fieldtype": "link","width": 100},
		{"label": ("Total Quantity"), "fieldname": "claim_date", "fieldtype": "float","width": 100},
		{"label": ("Total Net Weight"), "fieldname": "customer", "fieldtype": "float","width": 100},
		{"label": ("Weight To Be Manufactured"), "fieldname": "item_name", "fieldtype": "float","width": 100},
		{"label": ("Weight In Progress"), "fieldname": "serial_no", "fieldtype": "float","width": 100},
		{"label": ("Weight Manufactured"), "fieldname": "sale_price", "fieldtype": "float","width": 100},
		{"label": ("Weight delivered"), "fieldname": "claim_no", "fieldtype": "float","width": 100},
	]

	return columns

def get_result(filters):
	res = []
	entry = {
		"s":"d"
		}
	res.append(entry)
	return frappe.db.sql("""Select name,total_qty,total_net_weight,stapl_weight_to_be_manufactured,stapl_weight_in_progress,stapl_weight_manufactured,stapl_weight_delivered FROM `tabSales Order`""")