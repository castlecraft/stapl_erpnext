from . import __version__ as app_version

app_name = "stapl_erpnext"
app_title = "STAPL ERPNext"
app_publisher = "Castlecraft Ecommerce Pvt. Ltd."
app_description = "ERPNext customization for STAPL"
app_icon = "octicon octicon-file-directory"
app_color = "grey"
app_email = "support@castlecraft.in"
app_license = "GPLv3"

# Includes in <head>
# ------------------

# include js, css files in header of desk.html
# app_include_css = "/assets/stapl_erpnext/css/stapl_erpnext.css"
# app_include_js = "/assets/stapl_erpnext/js/stapl_erpnext.js"

# include js, css files in header of web template
# web_include_css = "/assets/stapl_erpnext/css/stapl_erpnext.css"
# web_include_js = "/assets/stapl_erpnext/js/stapl_erpnext.js"

# include custom scss in every website theme (without file extension ".scss")
# website_theme_scss = "stapl_erpnext/public/scss/website"

# include js, css files in header of web form
# webform_include_js = {"doctype": "public/js/doctype.js"}
# webform_include_css = {"doctype": "public/css/doctype.css"}

# include js in page
# page_js = {"page" : "public/js/file.js"}

# include js in doctype views
doctype_js = {"Sales Order" : "public/js/stapl_sales_order.js"}
# doctype_list_js = {"doctype" : "public/js/doctype_list.js"}
# doctype_tree_js = {"doctype" : "public/js/doctype_tree.js"}
# doctype_calendar_js = {"doctype" : "public/js/doctype_calendar.js"}

# Home Pages
# ----------

# application home page (will override Website Settings)
# home_page = "login"

# website user home page (by Role)
# role_home_page = {
#	"Role": "home_page"
# }

# Generators
# ----------

# automatically create page for each record of this doctype
# website_generators = ["Web Page"]

# Installation
# ------------

# before_install = "stapl_erpnext.install.before_install"
# after_install = "stapl_erpnext.install.after_install"

# Desk Notifications
# ------------------
# See frappe.core.notifications.get_notification_config

# notification_config = "stapl_erpnext.notifications.get_notification_config"

# Permissions
# -----------
# Permissions evaluated in scripted ways

# permission_query_conditions = {
# 	"Event": "frappe.desk.doctype.event.event.get_permission_query_conditions",
# }
#
# has_permission = {
# 	"Event": "frappe.desk.doctype.event.event.has_permission",
# }

# DocType Class
# ---------------
# Override standard doctype classes

# override_doctype_class = {
# 	"ToDo": "custom_app.overrides.CustomToDo"
# }

# Document Events
# ---------------
# Hook on document methods and events

# doc_events = {
# 	"*": {
# 		"on_update": "method",
# 		"on_cancel": "method",
# 		"on_trash": "method"
#	}
# }

# Scheduled Tasks
# ---------------

# scheduler_events = {
# 	"all": [
# 		"stapl_erpnext.tasks.all"
# 	],
# 	"daily": [
# 		"stapl_erpnext.tasks.daily"
# 	],
# 	"hourly": [
# 		"stapl_erpnext.tasks.hourly"
# 	],
# 	"weekly": [
# 		"stapl_erpnext.tasks.weekly"
# 	]
# 	"monthly": [
# 		"stapl_erpnext.tasks.monthly"
# 	]
# }

# Testing
# -------

# before_tests = "stapl_erpnext.install.before_tests"

# Overriding Methods
# ------------------------------
#
override_whitelisted_methods = {
	"erpnext.selling.doctype.sales_order.sales_order.make_raw_material_request": "stapl_erpnext.api.make_raw_material_request"
}
#
# each overriding function accepts a `data` argument;
# generated from the base implementation of the doctype dashboard,
# along with any modifications made in other Frappe apps
# override_doctype_dashboards = {
# 	"Task": "stapl_erpnext.task.get_dashboard_data"
# }

# exempt linked doctypes from being automatically cancelled
#
# auto_cancel_exempted_doctypes = ["Auto Repeat"]


# User Data Protection
# --------------------

user_data_fields = [
	{
		"doctype": "{doctype_1}",
		"filter_by": "{filter_by}",
		"redact_fields": ["{field_1}", "{field_2}"],
		"partial": 1,
	},
	{
		"doctype": "{doctype_2}",
		"filter_by": "{filter_by}",
		"partial": 1,
	},
	{
		"doctype": "{doctype_3}",
		"strict": False,
	},
	{
		"doctype": "{doctype_4}"
	}
]

# Authentication and authorization
# --------------------------------

# auth_hooks = [
# 	"stapl_erpnext.auth.validate"
# ]
fixtures = [
	{
        "dt": "Custom Field",
        "filters": [
            [
                "name",
                "in",
                [
                    "Sales Order-stapl_weight_to_be_manufactured",
					"Sales Order-stapl_weight_in_progress",
					"Sales Order-stapl_weight_manufactured",
					"Sales Order-stapl_weight_delivered",
					"Delivery Note-stapl_driver_contact_no"                                 
                ],
            ],
        ]
    },
	{
        "dt": "Property Setter",
        "filters": [
            [
                "name",
                "in",
                [
                    "Delivery Note-driver-Hidden",                                 
                ],
            ],
        ]
    },
]
