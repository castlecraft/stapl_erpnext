from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in stapl_erpnext/__init__.py
from stapl_erpnext import __version__ as version

setup(
	name='stapl_erpnext',
	version=version,
	description='ERPNext customization for STAPL',
	author='Castlecraft Ecommerce Pvt. Ltd.',
	author_email='support@castlecraft.in',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
